from scrapy.item import Item, Field


class Website(Item):
    name = Field()
    description = Field()
    url = Field()

    """
    TODO: Do you know how to fetch image from internet?
    """
    # picture = Field()

